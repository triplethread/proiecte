import numpy as np
import prettytable as prettytable
import matplotlib.pyplot as pyplot
import matplotlib.patches as patches
from smo_alg import SupportVectorMachine

roman = -1
nuvela = 1

TRAINING_DATA = [
    [["feature #1", "feature #2"], ["label"]],
    [[6.0, 6.0], [nuvela]],
    [[6.0, 5.0], [nuvela]],
    [[5.0, 4.0], [nuvela]],
    [[4.0, 3.0], [nuvela]],
    [[9.0, 3.0], [nuvela]],
    [[6.0, 6.0], [nuvela]],
    [[2.0, 10.0], [nuvela]],
    [[5.0, 3.0], [nuvela]],
    [[4.0, 3.0], [nuvela]],
    [[7.0, 15.0], [nuvela]],
    [[26.0, 13.0], [roman]],
    [[9.0, 50.0], [roman]],
    [[6.0, 12.0], [roman]],
    [[15.0, 29.0], [roman]],
    [[11.0, 20.0], [roman]],
    [[8.0, 17.0], [roman]],
    [[23.0, 22.0], [roman]],
    [[19.0, 36.0], [roman]],
    [[7.0, 25.0], [roman]],
    [[10.0, 16.0], [roman]],
]

def plot(x, y, w, b):
    x0 = []
    y0 = []
    x1 = []
    y1 = []
    for i in range(1, np.ma.size(x,0)):
        if y[i] == -1:
            x0.append(x[i][0])
            y0.append(x[i][1])
        else:
            x1.append(x[i][0])
            y1.append(x[i][1])

    plot = pyplot.figure()
    ax = plot.add_subplot(1, 1, 1)
    ax.set_title("SVM")
    pyplot.xlabel("Nr Personaje")
    pyplot.ylabel("Nr Capitole")

    ax.scatter(x0, y0, marker='o', s=30, c='orange', label='Romane')
    ax.scatter(x1, y1, marker='o', s=30, c='green', label='Nuvele')
    ax.legend()
    print w
    print b
    x = np.reshape(np.arange(0.0, 50.0, 0.1), (500,1))
    y = -np.multiply(w[0],x)
    y = y-b
    y = y/w[1]
    y = np.array(y)
    ax.plot(x, y)
    ax.axis([0, 50, 0, 50])
    pyplot.show()


x = []
y = []

for i_t in range(1, len(TRAINING_DATA)):
    x.append(TRAINING_DATA[i_t][0])
    y.append(TRAINING_DATA[i_t][1][0])
x = np.array(x)
y = ((np.array(y))[np.newaxis])
y = y.transpose()
svm = SupportVectorMachine(x,y)
dumb_w = np.array([-44.35244895, 1.50714969, 5.52834138])
b = svm.get_b()
w = svm.get_w()
plot(x, y, w, b)
