import numpy as np
import prettytable as prettytable
import matplotlib.pyplot as pyplot
import matplotlib.patches as patches


def plot(x,y,labelx,labely):
    x0 = []
    y0 = []
    x1 = []
    y1 = []
    for i in range(1, np.ma.size(x,0)):
        if y[i] == -1:
            x0.append(x[i][0])
            y0.append(x[i][1])
        else:
            x1.append(x[i][0])
            y1.append(x[i][1])

    plot = pyplot.figure()
    ax = plot.add_subplot(1, 1, 1)
    pyplot.xlabel(labelx)
    pyplot.ylabel(labely)

    ax.scatter(x0, y0, marker='o', s=30, c='orange')
    ax.scatter(x1, y1, marker='o', s=30, c='green')

    ax.axis([0, 10, 0, 10])
    pyplot.show()




TRAINING_DATA = [
    [["feature #1", "feature #2"], ["label"]],
    [[9.123, 3.123], [+1]],
    [[9.123, 5.123], [+1]],
    [[5.123, 5.123], [-1]],
    [[8.123, 6.654], [+1]],
    [[4.654, 4.123], [-1]],
    [[2.123, 4.123], [-1]],
    [[9.123, 7.123], [+1]],
    [[4.123, 4.654], [-1]],
    [[8.654, 2.123], [+1]],
    [[2.123, 2.123], [-1]],
    [[3.123, 3.123], [-1]],
]



x = []
y = []

for i_t in range(1, len(TRAINING_DATA)):
    x.append(TRAINING_DATA[i_t][0])
    y.append(TRAINING_DATA[i_t][1][0])
x = np.array(x)
y = np.array(y)


plot(x,y, "labelx", "labely")