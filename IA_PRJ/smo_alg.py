import numpy as np
import random
MAX_NUMB_OF_ITERATIONS = 50
C = 1
EPSILON = 0.0001
import math


class SupportVectorMachine:
    def __init__(self, x, y):
        self._x = x    # features
        self._y = y   # labels
        # set alpha to 0 to satisfy the stationary condition
        self._alpha = np.array(np.zeros((np.shape(x)[0], 1)))
        # b is 0 at the beginning
        self._b = np.array([[0]])

        i = 0
        while i < MAX_NUMB_OF_ITERATIONS:
            if self.perform_smo() == 0:
                i += 1
            else:
                i = 0
        print self._alpha
        self._w = self.calc_w(self._alpha, self._x, self._y)

    def __check_error_violation(self, E):
        if abs(E) > EPSILON:
            return True
        return False


    def __get_error_val(self, i):
        E = np.dot(np.multiply(self._y, self._alpha).T, self._x)
        E = np.dot(E, self._x[i].T) + self._b - self._y[i]
        E = E[0][0]
        return E

    def perform_smo(self):
        flag = False
        for i in range(np.ma.size(self._x, 0)):
            Ei = self.__get_error_val(i)
            if self.check_if_alpha_violates_kkt(self._alpha[i], Ei):
                j = self.__choose_second_alpha(i, np.ma.size(self._x, 0))
                Ej = self.__get_error_val(j)
                bounds = self.bound_alpha(self._alpha[i], self._alpha[j], self._y[i], self._y[j])

                ETA = 2.0 * np.dot(self._x[i] , self._x[j].T) - np.dot(self._x[i] , self._x[i].T) - np.dot(self._x[j] , self._x[j].T)
                if ETA == 0:
                    ETA = 100
                if self.optimize_alpha_pair(i, j, Ei, Ej, ETA, bounds, self._alpha[i].copy(),
                                            self._alpha[j].copy()):
                    flag = True

        return flag

    def __choose_second_alpha(self, i, nrRows):
        j = i
        while j == i:
            j = random.randint(0, nrRows-1)
        return j


    # this ensures that the alpha stays inside the cube limits
    def bound_alpha(self, alphai, alphaj, yi, yj):
        bounds = [2]
        if yi == yj:
            bounds.insert(0, max(0, alphaj + alphai - C))
            bounds.insert(1, min(C, alphaj + alphai))
        else:
            bounds.insert(0, max(0, alphaj - alphai))
            bounds.insert(1, min(C, alphaj - alphai + C))
        return bounds

    def calc_w(self, alpha, x, y):
        w = np.zeros(np.ma.size(self._x, 1))
        for i in range(np.ma.size(self._x, 0)):
            temp = np.array(np.multiply(y[i] * alpha[i], x[i].T))
            w = w+temp
        return w

    def optimize_alpha_pair(self, i, j, Ei, Ej, ETA, bounds, alphaIold, alphaJold):
        flag = False
        self._alpha[j] -= np.dot(self._y[j], (Ei - Ej) / ETA)
        if math.isnan(self._alpha[j]):
            print ETA
            pass
        self.clip_alpha_j(j, bounds)
        if abs(self._alpha[j]-alphaJold) > 0.001:
            self._alpha[i] += self._y[j] * self._y[i] * (alphaJold - self._alpha[j])
            if math.isnan(self._alpha[i]):
                print ETA
                pass
            self.__compute_b(Ei, Ej, alphaIold, alphaJold, i, j)
            flag = True
        return flag

    def clip_alpha_j(self, j, bounds):
        if self._alpha[j] < bounds[0]:
            self._alpha[j] = bounds[0]
        if self._alpha[j] > bounds[1]:
            self._alpha[j] = bounds[1]

    def check_if_alpha_violates_kkt(self, alpha, E):
        return (alpha > 0 and np.abs(E) < EPSILON) or (alpha < C and np.abs(E) > EPSILON)

    def __compute_b(self, Ei, Ej, alphaIold, alphaJold, i, j):
        tmp1 = np.dot(self._y[i],self._alpha[i] - alphaIold)
        tmp1 = np.dot(tmp1,self._x[i])
        tmp1 = np.dot(tmp1, self._x[i].T)
        tmp2 = np.dot(self._y[i],self._alpha[j] - alphaJold)
        tmp2 = np.dot(tmp2, self._x[i])
        tmp2 = np.dot(tmp2, self._x[j].T)

        b1 = self._b - Ei - tmp1 - tmp2

        tmp1 = np.dot(self._y[j], self._alpha[i] - alphaIold)
        tmp1 = np.dot(tmp1, self._x[i])
        tmp1 = np.dot(tmp1, self._x[j].T)
        tmp2 = np.dot(self._y[i], self._alpha[j] - alphaJold)
        tmp2 = np.dot(tmp2, self._x[j])
        tmp2 = np.dot(tmp2, self._x[j].T)

        b2 = self._b - Ej - tmp1 - tmp2

        if (0 < self._alpha[i]) and (C > self._alpha[i]):
            self._b = b1
        elif (0 < self._alpha[j]) and (C > self._alpha[j]):
            self._b = b2
        else:
            self._b = (b1 + b2) / 2.0

    def classify(self, x):
        classification = "classified as -1 "
        if np.sign((np.multiply(x, self._w) + self._b).item(0, 0)) == 1:
            classification = "classified as +1"
        return classification

    def get_alpha(self):
        return self._alpha

    def get_b(self):
        return self._b

    def get_w(self):
        return self._w
