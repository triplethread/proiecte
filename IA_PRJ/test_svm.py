import numpy as np
import prettytable as prettytable
import matplotlib.pyplot as pyplot
import matplotlib.patches as patches

TRAINING_DATA = [
    [["feature #1", "feature #2"], ["label"]],
    [[9.123, 3.123], [+1]],
    [[9.123, 5.123], [+1]],
    [[5.123, 5.123], [-1]],
    [[8.123, 6.654], [+1]],
    [[4.654, 4.123], [-1]],
    [[2.123, 4.123], [-1]],
    [[9.123, 7.123], [+1]],
    [[4.123, 4.654], [-1]],
    [[8.654, 2.123], [+1]],
    [[2.123, 2.123], [-1]],
    [[3.123, 3.123], [-1]],
]


def hypothesis(x, w):
    return np.sign(np.dot(w, x))
# Make predictions on all data points
# and return the ones that are misclassified.
def predict(hypothesis_function, X, y, w):
    predictions = np.apply_along_axis(hypothesis_function, 1, X, w)
    misclassified = X[y != predictions]
    return misclassified

# Pick one misclassified example randomly
# and return it with its expected label.
def pick_one_from(misclassified_examples, X, y):
    np.random.shuffle(misclassified_examples)
    x = misclassified_examples[0]
    index = np.where(np.all(X == x, axis=1))
    return x, y[index]

def perceptron_learning_algorithm(X, y):
    w = np.random.rand(3) # can also be initialized at zero.
    misclassified_examples = predict(hypothesis, X, y, w)
    while misclassified_examples.any():
        x, expected_y = pick_one_from(misclassified_examples, X, y)
        w = w + x * expected_y # update rule
        misclassified_examples = predict(hypothesis, X, y, w)
    return w



def plot(x, y, w):
    x0 = []
    y0 = []
    x1 = []
    y1 = []
    for i in range(1, np.ma.size(x,0)):
        if y[i] == -1:
            x0.append(x[i][1])
            y0.append(x[i][2])
        else:
            x1.append(x[i][1])
            y1.append(x[i][2])

    plot = pyplot.figure()
    ax = plot.add_subplot(1, 1, 1)
    pyplot.xlabel("X")
    pyplot.ylabel("Y")

    ax.scatter(x0, y0, marker='o', s=30, c='orange')
    ax.scatter(x1, y1, marker='o', s=30, c='green')

    x = np.arange(0.0, 10.0, 0.1)
    a = -w[1]/w[2]
    b = -w[0]/w[2]
    y = (a*x + b)
    ax.plot(x, y)
    ax.axis([0, 10, 0, 10])
    pyplot.show()




x = []
y = []

for i_t in range(1, len(TRAINING_DATA)):
    x.append(TRAINING_DATA[i_t][0])
    y.append(TRAINING_DATA[i_t][1][0])
x = np.array(x)
y = np.array(y)

x = np.c_[np.ones(x.shape[0]), x]

print x
w = perceptron_learning_algorithm(x, y)
print(w) # [-44.35244895 1.50714969 5.52834138]

plot(x,y,w)