/*
 ============================================================================
 Name        : laborator1.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello MPI World in C 
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include "mpi.h"
#include <math.h>
#include <time.h>
#include <stdlib.h>

MPI_Comm comm_cart= NULL;
MPI_Status status ;   /* return status for receive */
int  my_rank; /* rank of process */
// ndims = 2
int ndims = 2;

int** create_matrix(int n)
{
	int **A = (int**)malloc(n*sizeof(int*));
	for(int i=0;i<n;i++)
	{
		A[i] = (int*)malloc(n*sizeof(int));
	}


	for(int i=0;i<n;i++)
	{
		for(int j=0;j<n;j++)
		{
			A[i][j] = (rand()%10 + i + j%10)%2;
		}
	}

	return A;
}

int* matrix_to_vector(int **A,int n)
{
	int *ret = (int*)malloc(n*n*sizeof(int));
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<n;j++)
		{
			ret[i*n+j] = A[i][j];
		}
	}
	return ret;
}

void print_matrix(int *A,int q)
{
	int n=q*q;
	int coords[2];
	if(my_rank == 0)
	{
		int real_matrix[n][n];
		int received[n];
		for(int i=1;i<n;i++)
		{
			//printf("Astept alt procesor sa imi trimita %d!\n",i);
			MPI_Recv(received,n,MPI_INT,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
			int source_rank = status.MPI_SOURCE;
			//printf("<< Am primit matricea de la procesorul %d!\n",source_rank);
			MPI_Cart_coords(comm_cart,source_rank,ndims,coords);
			int real_coords[2] = {coords[0]*q,coords[1]*q};
			for(int j=real_coords[0];j<real_coords[0]+q;j++)
			{
				for(int k=real_coords[1];k<q+real_coords[1];k++)
				{
					real_matrix[j][k]= received[(j%q)*q+(k%q)];
				}
			}
		}
		
		MPI_Cart_coords(comm_cart,my_rank,ndims,coords);
		int real_coords[2] = {coords[0]*q,coords[1]*q};
			for(int j=real_coords[0];j<real_coords[0]+q;j++)
			{
				for(int k=real_coords[1];k<q+real_coords[1];k++)
				{
					real_matrix[j][k]= A[(j%q)*q+(k%q)];
					//real_matrix[j][k] = -1;
				}
			}
		
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<n;j++)
			{
				printf("%2d ",real_matrix[i][j]);
				if(j>0 && (j+1)%q ==0)
				{
					printf(" : ");
				}
			}
			if(i>0 && (i+1)%q == 0)
			{
				printf("\n");
				for(int j=0;j<=n;j++)
				{
					printf(" -- ");
				}
			}
			printf("\n");
		}

	}
	else
	{
		//printf(">> Procesorul %d a trimis matricea!\n",my_rank);
		MPI_Send(A,n,MPI_INT,0,0,MPI_COMM_WORLD);
	}

}
/////////////////////////////////////////////////////////
void trimite_la_stanga(int *A,int q)
{
	int coords[2];
	int neighbor_rank;
	coords[0] = i;
	coords[1] = (j+q-1)%q;
	MPI_Cart_rank(comm_cart,coords,&neighbor_rank);
	MPI_Send(A, q*q, MPI_INT, neighbor_rank, 0, comm_cart);
}
void trimite_sus(int *A,int q)
{
	int coords[2];
	int neighbor_rank;
	coords[0] = (i-1 +q)%q;
	coords[1] = j;
	MPI_Cart_rank(comm_cart,coords,&neighbor_rank);
	MPI_Send(A, q*q, MPI_INT, neighbor_rank, 0, comm_cart);
}
void primeste_la_stanga(int *A,int q)
{
	int coords[2];
	int neighbor_rank;
	coords[0] = i;
	coords[1] = (j+1)%q;
	MPI_Cart_rank(comm_cart,coords,&neighbor_rank);
	MPI_Recv(A, q*q, MPI_INT, neighbor_rank, MPI_ANY_TAG, comm_cart,&status);	
}
void primeste_sus(int *A,int q)
{
	int coords[2];
	int neighbor_rank;
	coords[0] = (i+1)%q;
	coords[1] = j;
	MPI_Cart_rank(comm_cart,coords,&neighbor_rank);
	MPI_Recv(B, q*q, MPI_INT, neighbor_rank, MPI_ANY_TAG, comm_cart,&status);	
}
//////////////////////////////////////////////////////////////////////////

void aliniere_initiala(int *A,int *B,int q,int i,int j)
{
	int neighbor_rank;
	int coords[2];
	
	for(int k=1;k<=i;k++)
	{
		coords[0] = i;
		coords[1] = (j+q-1)%q;
		MPI_Cart_rank(comm_cart,coords,&neighbor_rank);
		//printf(">> Procesorul %d %d a trimis catre %d %d.\n",i,j,coords[0],coords[1]);
		MPI_Send(A, q*q, MPI_INT, neighbor_rank, 0, comm_cart);

		coords[0] = i;
		coords[1] = (j+1)%q;
		MPI_Cart_rank(comm_cart,coords,&neighbor_rank);
		//printf("*<< Procesorul %d %d a primit de la %d %d.\n",i,j,coords[0],coords[1]);
		MPI_Recv(A, q*q, MPI_INT, neighbor_rank, MPI_ANY_TAG, comm_cart,&status);
	}
	for(int k=1;k<=j;k++)
	{		
		coords[0] = (i-1 +q)%q;
		coords[1] = j;
		MPI_Cart_rank(comm_cart,coords,&neighbor_rank);
		//printf(">> Procesorul %d %d a trimis catre %d %d.\n",i,j,coords[0],coords[1]);
		MPI_Send(B, q*q, MPI_INT, neighbor_rank, 0, comm_cart);


		coords[0] = (i+1)%q;
		coords[1] = j;
		MPI_Cart_rank(comm_cart,coords,&neighbor_rank);
		//printf("*<< Procesorul %d %d a primit de la %d %d.\n",i,j,coords[0],coords[1]);
		MPI_Recv(B, q*q, MPI_INT, neighbor_rank, MPI_ANY_TAG, comm_cart,&status);	
	}
	
}

int *inmultire_matrice(int *A,int* B,int q)
{
	int n= q*q;
	int *C = (int*)malloc(n*sizeof(int));
	int element;
	for(int i=0;i<q;i++)
	{
		for(int k=0;k<q;k++)
		{
			element=0;
			for(int j=0;j<q;j++)
			{
				element += A[i*q+j] * B[j*q+k];
			}
			C[i*q+k] = element;
		}
	}
	return C;
}

int* insumare_matrice(int *A,int *B,int q)
{
	int n = q*q;
	int *C = (int*)malloc(n*sizeof(int));
	for(int i=0;i<q;i++)
	{
		for(int j=0;j<q;j++)
		{
			C[i*q+j] = A[i*q+j] + B[i*q+j];
		}
	}
	return C;
}

int* inmultire_Cannon(int *A,int *B,int n,int q,int i,int j)
{
	aliniere_initiala(A,B,q,i,j);
	int *C = (int*)malloc(n*sizeof(int));
	for(int i=0;i<n;i++)
		C[i] = 0;

	for(int k=0;k<q-2;k++)
	{
		C = insumare_matrice(C,inmultire_matrice(A,B,q),q);
	}


}


int main(int argc, char* argv[]){
	
	int  p;       /* number of processes */
	int source;   /* rank of sender */
	int dest;     /* rank of receiver */
	int tag=0;    /* tag for messages */
	char message[100];        /* storage for message */
	int coords[2];
	/* start up MPI */
	
	MPI_Init(&argc, &argv);
	
	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 
	
	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &p); 

	srand(time(NULL) + my_rank);

	// dims = [q,q]
	int dims[] = {0,0};

	// periods = [1,1] // ca sa faca legatura (primul cu ultimul pe linie si coloana)
	int periods[] = {1,1};

	int reorder = 0;
	MPI_Dims_create(p,2,dims);
	MPI_Cart_create(MPI_COMM_WORLD, ndims,dims,periods,reorder,&comm_cart);

	MPI_Cart_coords(comm_cart,my_rank,ndims,coords);

	int q = sqrt(p);
	int **A = create_matrix(q);
	int **B = create_matrix(q);
	int *v_A = matrix_to_vector(A,q);
	int *v_B = matrix_to_vector(B,q);

	if(my_rank == 0 )
	{
		printf("Matricea initiala:\n");
	}
	MPI_Barrier(MPI_COMM_WORLD);
	print_matrix(v_A,q);

	aliniere_initiala(v_A,v_B,q,coords[0],coords[1]);
	if(my_rank == 0)
	{
		printf("Matrice dupa ce s`a facut alinierea initiala!\n");
	}
	MPI_Barrier(MPI_COMM_WORLD);
	print_matrix(v_A,q);


	/* shut down MPI */
	MPI_Finalize(); 
	
	
	return 0;
}
