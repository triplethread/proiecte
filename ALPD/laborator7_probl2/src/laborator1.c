/*
 ============================================================================
 Name        : laborator1.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello MPI World in C 
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include "mpi.h"

int main(int argc, char* argv[]){
	int  my_rank; /* rank of process */
	int  p;       /* number of processes */
	int source;   /* rank of sender */
	int dest;     /* rank of receiver */
	int tag=0;    /* tag for messages */
	char message[100];        /* storage for message */
	MPI_Status status ;   /* return status for receive */
	
	/* start up MPI */
	
	MPI_Init(&argc, &argv);
	
	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 
	
	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &p); 
	

	/*
	Date de intrare pentru algoritm	
	*/
	int A[6] = {3,2,1,6,4,8};

	
	for(int i=0;i<p;i++)
	{
		// faza PARA
		if((i&0x01) == 0x00)
		{
			if(my_rank == 0)
				printf("*Runda PARA!\n");
			MPI_Barrier(MPI_COMM_WORLD);
			if(my_rank%2 ==0)
			{
				//comunica cu fratele + 1
				int val;
				if(my_rank+1<p){
					MPI_Send(A+my_rank,1,MPI_INT,my_rank+1,0,MPI_COMM_WORLD);
					printf(">>Procesorul %d a trimis mesaj procesorului %d.(%d)\n",my_rank,my_rank+1,A[my_rank]);
					MPI_Recv(&val,1,MPI_INT,my_rank+1,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
					printf("<<Procesorul %d a primit mesaj de la procesorul %d.(%d)\n",my_rank,my_rank+1,val);
					if(val < A[my_rank])
						A[my_rank] = val;
				}

			}
			else
			{
				//comunica cu fratele - 1
				int val;
				if(my_rank-1>=0){
					MPI_Send(A+my_rank,1,MPI_INT,my_rank-1,0,MPI_COMM_WORLD);
					printf(">>Procesorul %d a trimis mesaj procesorului %d.(%d)\n",my_rank,my_rank+1,A[my_rank]);
					MPI_Recv(&val,1,MPI_INT,my_rank-1,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
					printf("<<Procesorul %d a primit mesaj de la procesorul %d.(%d)\n",my_rank,my_rank-1,val);
					if(val > A[my_rank])
						A[my_rank] = val;
				}

			}
		}
		// faza impara
		else
		{
			if(my_rank == 0)
				printf("*Runda IMPARA!\n");
			MPI_Barrier(MPI_COMM_WORLD);

			if(my_rank%2 ==1)
			{
				//comunica cu fratele + 1
				int val;
				if(my_rank+1 < p){
					MPI_Send(A+my_rank,1,MPI_INT,my_rank+1,0,MPI_COMM_WORLD);
					printf(">>Procesorul %d a trimis mesaj procesorului %d.(%d)\n",my_rank,my_rank+1,A[my_rank]);
					MPI_Recv(&val,1,MPI_INT,my_rank+1,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
					printf("<<Procesorul %d a primit mesaj de la procesorul %d.(%d)\n",my_rank,my_rank+1,val);
					if(val < A[my_rank])
						A[my_rank] = val;
				}
			}
			else
			{
				//comunica cu fratele - 1
				int val;
				if(my_rank-1>=0){
					MPI_Send(A+my_rank,1,MPI_INT,my_rank-1,0,MPI_COMM_WORLD);
					printf(">>Procesorul %d a trimis mesaj procesorului %d.(%d)\n",my_rank,my_rank-1,A[my_rank]);
					MPI_Recv(&val,1,MPI_INT,my_rank-1,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
					printf("<<Procesorul %d a primit mesaj de la procesorul %d.(%d)\n",my_rank,my_rank-1,val);
					if(val > A[my_rank])
						A[my_rank] = val;
				}
			}
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	if(my_rank == 0)
	{
		printf("%d ",A[0]);
		for(int i=1;i<p;i++)
		{
			MPI_Recv(A+i,1,MPI_INT,i,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
			printf("%d ",A[i]);
		}
		printf("\n");

	}
	else{
		MPI_Send(A+my_rank,1,MPI_INT,0,0,MPI_COMM_WORLD);
	}
	/* shut down MPI */
	MPI_Finalize(); 
	
	
	return 0;
}
