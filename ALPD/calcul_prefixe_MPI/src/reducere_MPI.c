/*
 ============================================================================
 Name        : laborator6.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello MPI World in C 
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include "mpi.h"
#include "math.h"
#define problema1
#define problema2
int main(int argc, char* argv[]){
	int  my_rank; /* rank of process */
	int  p;       /* number of processes */
	int source;   /* rank of sender */
	int dest;     /* rank of receiver */
	int tag=0;    /* tag for messages */
	MPI_Status status ;   /* return status for receive */
	/* start up MPI */
	
	//////////////

	//////////////////
	MPI_Init(&argc, &argv);
	
	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 
	
	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &p); 

	/*
	Date de intrare
	*/

	// trebuie 2*n procese
	int n = 8;
	int A[2*n];
	for(int i=n;i<2*n;i++)
	{
		A[i] = i-n+1;
	}

	#ifdef problema1
	// problema 1 :
	if( my_rank!=0)
	{
		if(my_rank >= p/2)// inseamna ca e frunza
		{
			// send A[id] -> id/2
			MPI_Send(A+my_rank,1,MPI_INT,my_rank/2,0,MPI_COMM_WORLD);
			fprintf(stdout,">>Procesorul %d a trimis mesaj la procesorul %d cu continutul %d.\n",my_rank,my_rank/2,*(A+my_rank));
		}
		else {	// inseamna ca e nod intermediar sau radacina
			int a,b;
			// receive from a from id*2, b from id*2+1
			MPI_Recv(&a,1,MPI_INT,my_rank*2,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
			fprintf(stdout,"<<Procesorul %d a primit mesajul de la procesorul %d (%d).\n",my_rank,my_rank*2,a);
			MPI_Recv(&b,1,MPI_INT,my_rank*2+1,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
			fprintf(stdout,"<<Procesorul %d a primit mesajul de la procesorul %d (%d).\n",my_rank,my_rank*2+1,b);

			// a[id] = a + b
			A[my_rank] = a+b;
			// if(id!=1) send a[id] -> id/2
			if(my_rank != 1)
			{
				MPI_Send(A+my_rank,1,MPI_INT,my_rank/2,0,MPI_COMM_WORLD);	
			fprintf(stdout,">>Procesorul %d a trimis mesaj la procesorul %d cu continutul %d.\n",my_rank,my_rank/2,*(A+my_rank));
	
			}
		}
	}
	if(my_rank == 1)
	{
		printf("* Procesor %d: Mesajele se reduc la %d\n\n",my_rank,A[my_rank]);
	}
#endif

MPI_Barrier(MPI_COMM_WORLD);
#ifdef problema2
	// problema 2 :
	int B[2*n];

	if( my_rank !=0 )
	{
		if(my_rank == 1)
		{
			// B[1] = A[1] // B[id] = A[id]
			B[my_rank] = A[my_rank];

		}
		else
		{
			// receive a from id/2
			int a,b;
			MPI_Recv(&a,1,MPI_INT,my_rank/2,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
			fprintf(stdout,"<<Procesorul %d a primit mesajul de la procesorul %d - (%d).\n",my_rank,my_rank/2,a);
			// if( id&0x01 == 0x00)
			/*{	receive b from id+1
			 * B[id] = a + (-b)
			 * }
			 * else{
			 * send A[id] to id-1
			 * B[id]=a
			 * }
			 *
			 * if(id<p/2){
			 * send B[id] to id*2 and id*2+1
			 * }
			 */
			if( (my_rank & 0x01) == 0x00)
			{
				MPI_Recv(&b,1,MPI_INT,my_rank+1,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
				fprintf(stdout,"<<Procesorul %d a primit mesajul de la procesorul %d (%d).\n",my_rank,my_rank+1,b);
				B[my_rank] = a + (-b);
			}
			else
			{
				MPI_Send(A+my_rank,1,MPI_INT,my_rank-1,0,MPI_COMM_WORLD);
				fprintf(stdout,">>Procesorul %d a trimis mesaj la procesorul %d cu continutul %d.\n",my_rank,my_rank-1,*(A+my_rank));
				B[my_rank] = a;
			}
		}

		if(my_rank < p/2)
		{
			MPI_Send(B+my_rank,1,MPI_INT,2*my_rank,0,MPI_COMM_WORLD);
			fprintf(stdout,">>Procesorul %d a trimis mesaj la procesorul %d cu continutul %d.\n",my_rank,my_rank*2,*(B+my_rank));
			MPI_Send(B+my_rank,1,MPI_INT,2*my_rank+1,0,MPI_COMM_WORLD);
			fprintf(stdout,">>Procesorul %d a trimis mesaj la procesorul %d cu continutul %d.\n",my_rank,my_rank*2+1,*(B+my_rank));
		}
	}
#endif

	/* shut down MPI */
	MPI_Finalize(); 
	
	
	return 0;
}
