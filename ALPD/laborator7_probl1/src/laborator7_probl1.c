/*
 ============================================================================
 Name        : laborator7_probl1.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello OpenMP World in C
 ============================================================================
 */
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
/**
 * Hello OpenMP World prints the number of threads and the current thread id
 */
int main(int argc, char *argv[]) {

	int n = 4;
	int A[] = { 2, 6, 3, 8 };
	int R[2 * n - 2][n];

	omp_set_num_threads(n);

	for (int i = 0; i < 2 * n - 2; i++) {
		for (int j = 0; j < n; j++)
			R[i][j] = 0;
	}

#pragma omp parallel
	{

		int tid = omp_get_thread_num();
		//printf("Salut de la threadul %d.(%d)\n",tid,A[tid]);
		for (int i = 0; i < n; i++) {

			if (A[tid] < A[i]) {
				R[tid+n][i]=1;
				printf("Threadul %d a pus R[%d][%d] pe 1.\n",tid,(tid+n),i);
			}
			else{
				R[tid+n][i]=0;
				printf("Threadul %d a pus R[%d][%d] pe 0.\n",tid,tid+n,i);
			}
		}

#pragma omp barrier
		tid = omp_get_thread_num();
		for(int i=0;i<n;i++)
		{
			R[0][tid] += R[i+n][tid];
		}
	}

	for (int i = 0; i < 2 * n - 1; i++) {
			for (int j = 0; j < n; j++)
				printf("%d ",R[i][j]);
			printf("\n");
		}

	return 0;
}

