/*
 ============================================================================
 Name        : Difuzie_1_toti_hipercub.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello MPI World in C 
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include "mpi.h"

int max(int a,int b)
{
	if(a>b)
		return a;
	return b;
}
int min(int a,int b)
{
	if (max(a,b) == a)
		return b;
	return a;
}

int main(int argc, char* argv[]){
	int  my_rank; /* rank of process */
	int  p;       /* number of processes */
	int tag=0;    /* tag for messages */
	char M[100];        /* storage for message */
	MPI_Status status ;   /* return status for receive */
	int A[] = {1,5,3,2,6,7,5,4,1,1,2,2,5,3,4,5};
	int received;
	/* start up MPI */
	MPI_Init(&argc, &argv);
	
	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 
	
	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &p); 
	
	/*
	 * Informatii de intrare
	 */

	MPI_Send(A+my_rank,1,MPI_INT,my_rank^1,0,MPI_COMM_WORLD);
	MPI_Recv(&received,1,MPI_INT,my_rank^1,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
	if((my_rank & 0x010)==0)
	{
		if((my_rank & 0x01)==0)
		{
			A[my_rank] = max(A[my_rank],received);
		}
		else
		{
			A[my_rank] = min(A[my_rank],received);
		}
	}
	else
	{		
		if((my_rank & 0x01)==1)
		{
			A[my_rank] = max(A[my_rank],received);
		}
		else
		{
			A[my_rank] = min(A[my_rank],received);
		}
	}



	MPI_Finalize(); 

	
	return 0;
}
