/*
 ============================================================================
 Name        : laborator6.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello MPI World in C 
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include "mpi.h"
#include "math.h"

int main(int argc, char* argv[]){
	int  my_rank; /* rank of process */
	int  p;       /* number of processes */
	int source;   /* rank of sender */
	int dest;     /* rank of receiver */
	int tag=0;    /* tag for messages */
	char message[100];        /* storage for message */
	MPI_Status status ;   /* return status for receive */
	/* start up MPI */
	
	//////////////

	//////////////////
	MPI_Init(&argc, &argv);
	
	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 
	
	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &p); 
	
	if( my_rank!=0)
	{
		if(my_rank >= p/2)// inseamna ca e frunza
		{
			// send A[id] -> id/2
		}
		else {	// inseamna ca e nod intermediar sau radacina

			// receive from a from id*2, b from id*2+1
			// a[id] = a + b
			// if(id!=1) send a[id] -> id/2
		}
	}

	// problema 2 :
	if( my_rank !=0 )
	{
		if(my_rank == 1)
		{
			// B[1] = A[1] // B[id] = A[id]
		}
		else
		{
			// receive a from id/2
			// if( id&0x01 == 0x00)
			/*{	receive b from id+1
			 * B[id] = a + (-b)
			 * }
			 * else{
			 * send A[id] to id-1
			 * B[id]=a
			 * }
			 *
			 * if(id<p/2){
			 * send B[id] to id*2 and id*2+1
			 * }
			 */

		}
	}

	/* shut down MPI */
	MPI_Finalize(); 
	
	
	return 0;
}
