/*
 ============================================================================
 Name        : Difuzie_1_toti_hipercub.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello MPI World in C 
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include "mpi.h"

int main(int argc, char* argv[]){
	int  my_rank; /* rank of process */
	int  p;       /* number of processes */
	int tag=0;    /* tag for messages */
	char M[100];        /* storage for message */
	MPI_Status status ;   /* return status for receive */
	
	/* start up MPI */
	MPI_Init(&argc, &argv);
	
	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 
	
	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &p); 
	
	/*
	 * Informatii de intrare
	 */
	if(my_rank == 0 )
		strcpy(M,"Hello to all"); // mesaj

	printf("**Procesorul %d are mesajul initial %s.\n",my_rank,M);
	MPI_Barrier(MPI_COMM_WORLD);
	int d = 3;	// dimensiune hipercub
	int mask = (1<<3) -1;
	for(int k = d-1;k>=0;k--)
	{
		mask = (mask ^ (1<<k));
		if ( (my_rank & mask) == 0)
		{
			if( (my_rank&(1<<k)) == 0 )
			{
				// expeditor
				int destinatie = my_rank ^ (1<<k);
				MPI_Send(M,strlen(M)+1,MPI_CHAR,destinatie,tag,MPI_COMM_WORLD);
				printf(">Procesorul %d a trimis mesajul la destinatia %d.\n",my_rank,destinatie);

			}
			else{
				// destinatar
				int sursa = my_rank ^ (1<<k);
				MPI_Recv (M,100,MPI_CHAR,sursa,tag,MPI_COMM_WORLD,&status);
				printf("<Procesorul %d a primit mesajul la sursa %d.\n",my_rank,sursa);
			}
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}

printf("Procesorul %d are mesajul %s.\n",my_rank,M);
	/* shut down MPI */
	MPI_Finalize(); 

	
	return 0;
}
