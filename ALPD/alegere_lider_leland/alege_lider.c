/*
 ============================================================================
 Name        : laborator1.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello MPI World in C 
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include "mpi.h"

MPI_Comm comm_cart= NULL;

int  my_rank; /* rank of process */
int  p;       /* number of processes */
MPI_Status status ;   /* return status for receive */

void LCR_LE(char *statut,int *lider)
{
	/*****************************************/
	int coords;
	int source;
	int destination;
	int received_rank;
	coords = (my_rank+p-1)%p;
	MPI_Cart_rank(comm_cart,&coords,&source);
	printf("Sursa lui %d este %d\n",my_rank,source);
	coords = (my_rank+1)%p;
	MPI_Cart_rank(comm_cart,&coords,&destination);
	printf("Destinatia lui %d este %d\n",my_rank,destination);
	/*********************************************************/
	MPI_Send(&my_rank, 1, MPI_INT, destination ,'N', comm_cart);
	printf("(1) >>Procesorul %d a trimis propriul rank la %d!\n",my_rank,destination);

	while(1)
	{
		MPI_Recv(&received_rank, 1, MPI_INT, source, MPI_ANY_TAG, comm_cart, &status);
		printf("<<Procesorul %d a primit rank`ul %d cu tagul %c\n",my_rank,received_rank,status.MPI_TAG );
		if(status.MPI_TAG == 'N')
		{
			if(received_rank == my_rank)
			{
				// sunt lider
				*statut = 'L';
				*lider = my_rank;
				// informez pe toata lumea
				MPI_Send(&my_rank,1,MPI_INT, destination,'L',comm_cart);
				break;
			}
			else if (received_rank > my_rank)
			{
				// trimit mai departe
				MPI_Send(&received_rank,1,MPI_INT, destination, 'N', comm_cart);
				printf("// >>Procesorul %d a trimis mai departe rankul %d la %d!\n",my_rank,received_rank,destination);
			}
		}
		else if(status.MPI_TAG == 'L')
		{
			// nu sunt lider
			*statut = 'N';
			// tin minte id`ul liderului
			*lider = received_rank;
			// informez pe vecinul meu cine e liderul
			MPI_Send(lider,1,MPI_INT, destination, 'L', comm_cart);
			break;
		}

	}
}

int main(int argc, char* argv[]){	
	/* start up MPI */
	
	MPI_Init(&argc, &argv);
	
	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 
	
	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &p); 
		

	// dims = [q,q]
	int dims[] = {0,0};

	// periods = [1,1] // ca sa faca legatura (primul cu ultimul pe linie si coloana)
	int periods[] = {1,1};

	int reorder = 0;
	MPI_Dims_create(p,1,dims);
	MPI_Cart_create(MPI_COMM_WORLD, 1,dims,periods,reorder,&comm_cart);


	char statut;
	int lider;
	LCR_LE(&statut,&lider);
	printf("** Procesorul %d stie statut = %c si lider = %d\n",my_rank,statut,lider);

	


	/* shut down MPI */
	MPI_Finalize(); 
	
	
	return 0;
}
