/*
 ============================================================================
 Name        : laborator1.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello MPI World in C 
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include "mpi.h"

int main(int argc, char* argv[]){
	int  my_rank; /* rank of process */
	int  p;       /* number of processes */
	int source;   /* rank of sender */
	int dest;     /* rank of receiver */
	int tag=0;    /* tag for messages */
	char message[100];        /* storage for message */
	MPI_Status status ;   /* return status for receive */
	
	/* start up MPI */
	
	MPI_Init(&argc, &argv);
	
	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 
	
	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &p); 
	

	/*
	Date de intrare pentru algoritm	
	*/
	float A[][3] = {{1,2,2},{1,4,3},{1,3,2}}; // x- 1, y - 1
	float B[] = {5,8,6};



	//printf("####### %f %f %f",A[my_rank][0],A[my_rank][1],A[my_rank][2]);
	for(int i=0;i<p-1;i++)
	{
		if(my_rank == i){
			for(int j=i+1;j<p;j++)
			{
				A[my_rank][j] = A[my_rank][j]/ A[my_rank][my_rank];
				
			}
			B[my_rank] = B[my_rank]/A[my_rank][my_rank];			
			A[my_rank][my_rank] = 1;


			// trimit cei j coeficienti
			for(int j=i+1;j<p;j++)
			{
				// trimit coeficientul A[my_rank][j] la toate cele k procesoare care asteapta
				for(int k=i+1;k<p;k++)
				{
					MPI_Send(A[my_rank]+j,1,MPI_FLOAT,k,j,MPI_COMM_WORLD);
					//printf(">>Procesorul %d a trimis mesaj procesorului %d.(%f)\n",my_rank,k,A[my_rank][j]);
				}
				// trebuie trimis si coeficientul pentru matricea B
				MPI_Send(B+my_rank,1,MPI_FLOAT,j,0,MPI_COMM_WORLD);
				//printf(">>* Procesorul %d a trimis mesaj procesorului %d.(%f)\n",my_rank,j,B[my_rank]);
			}
			
		}
		else if(my_rank>i){
			float i_proc_coef;
			// astept coeficientii ce mi`au fost trimisi
			for(int j=i+1;j<=p;j++)
			{
				MPI_Recv(&i_proc_coef,1,MPI_FLOAT,i,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
				int coef_nr = status.MPI_TAG;
				//printf("<<Procesorul %d a primit mesaj de la procesorul %d cu mesajul %f, coeficientul numarul %d.\n",my_rank,i,i_proc_coef,coef_nr);
				if(coef_nr > 0)
				{
					//printf("** <%d> %f = %f - %f*%f\n",my_rank,A[my_rank][coef_nr],A[my_rank][coef_nr],A[my_rank][i],i_proc_coef);
					A[my_rank][coef_nr] =A[my_rank][coef_nr]- A[my_rank][i] * i_proc_coef;
				}
				else{
					B[my_rank] = B[my_rank] - A[my_rank][my_rank-1]*i_proc_coef;
				}
			}
			A[my_rank][i] = 0;

		}
	}

	MPI_Barrier(MPI_COMM_WORLD);
	if(my_rank>0){
		for(int i=0;i<p;i++)
		{
			MPI_Send(A[my_rank]+i,1,MPI_FLOAT,0,i,MPI_COMM_WORLD);
		}
		MPI_Send(B+my_rank,1,MPI_FLOAT,0,100,MPI_COMM_WORLD);
	}
	else{
		for(int i=0;i<(p-1)*(p+1);i++)
		{
			float val;
			int tag,rank;
			MPI_Recv(&val,1,MPI_FLOAT,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
			tag = status.MPI_TAG;
			rank = status.MPI_SOURCE;
			
			//printf("(dbg)Procesorul %d a primit mesaj de la procesorul %d cu mesajul %f, coeficientul numarul %d.\n",my_rank,rank,val,tag);
			if(tag!=100)
			{
				A[rank][tag] = val;
			}
			else
			{
				B[rank] = val;
			}
		}

	}
	if(my_rank==0)
	{
		for(int i=0;i<p;i++)
		{
			for(int j=0;j<p;j++)
			{
				printf("%f ",A[i][j]);
			}
			printf("| %f ",B[i]);
			printf("\n");
		}
	}
	/* shut down MPI */
	MPI_Finalize(); 
	
	
	return 0;
}
