/*
 ============================================================================
 Name        : difuzie_1_toti_arbore.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello MPI World in C 
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include "mpi.h"

int main(int argc, char* argv[]) {
	int my_rank; /* rank of process */
	int p; /* number of processes */
	char message[100]; /* storage for message */
	MPI_Status status; /* return status for receive */

	/* start up MPI */

	MPI_Init(&argc, &argv);

	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &p);

	/*
	 * Date intrare
	 */
	int nnodes = 4;
	int index[] = {2,3,7,8,9,10};
	int edges[] = { 1,2,0,3,4,5,0,2,2,2};
	int reorder = 1;
	int my_neighbors_count;
	int my_neighbors[10];
	MPI_Comm vu;
	// cream graful
	if(my_rank<4){
	MPI_Graph_create(MPI_COMM_WORLD, nnodes, index, edges, reorder, &vu);


	MPI_Graph_neighbors_count(vu, my_rank, &my_neighbors_count);
	MPI_Graph_neighbors(vu, my_rank,10, my_neighbors);

	if (my_rank == 0) {
		char root_message[] = "Hello dudes!";
		for(int i=0;i<my_neighbors_count-1;i++)
		{
			if(my_neighbors[i] != my_rank)
			{
				MPI_Send(root_message,strlen(root_message)+1,MPI_CHAR,my_neighbors[i],0,vu);
				fprintf(stdout,">>Procesorul %d a trimis mesajul catre procesorul %d.\n",my_rank,my_neighbors[i]);
			}
		}

	} else {
			MPI_Recv(message,100,MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,vu,&status);
			int source = status.MPI_SOURCE;
			fprintf(stdout,"<<Procesorul %d a primit mesajul de la procesorul %d.\n",my_rank,source);

			for(int i=0;i<my_neighbors_count-1;i++)
			{
				if(my_neighbors[i] != source)
				{
					MPI_Send(message,strlen(message)+1,MPI_CHAR,my_neighbors[i],0,vu);
					fprintf(stdout,">>Procesorul %d a trimis mesajul catre procesorul %d.\n",my_rank,my_neighbors[i]);
				}
			}
	}
	}
	fflush(stdout);

	/* shut down MPI */
	MPI_Finalize();

	return 0;
}
