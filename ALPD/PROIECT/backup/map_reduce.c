/*
 ============================================================================
 Name        : laborator1.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello MPI World in C 
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "mpi.h"

/********************* COMMON VARIABLES ***************************/
int  p;       /* number of processes */
int nr_of_files = 5;
char files[][80] = {"a","b","c","d","e"};
char path[] = "input_files";
char *full_path;

MPI_Status status ;   /* return status for receive */
int tag=0;    /* tag for messages */
int  my_rank; /* rank of process */
int index_of_file;
int nr_of_slave_files;

typedef struct{
	char** terms;
	int *count;
	int nr_of_terms;
}parsed_data;
/******************************************************************/

#define nullptr 0
/******************************************************************/

int* dispatch_tasks()
{
	int *A = (int*)malloc(2*sizeof(int));
	char temp[80];
	full_path = (char*)malloc( strlen(path) + strlen("/_process") +2);

	strcpy(full_path,path);
	strcat(full_path,"/_process");		

	if (my_rank ==0){	// master
		/* create message */

		/******************************************************/
		/************ Organize the work for the slaves ********/
		/******************************************************/
		int nr_max_slaves = p-1;
		int d = 0 , r = 0;
		
		d = nr_of_files / nr_max_slaves;
		r = nr_of_files % nr_max_slaves;

		index_of_file = 0;
		nr_of_slave_files = index_of_file + d + (int)(r>0);
		for(int i=1;i<=nr_max_slaves;i++)
		{
			A[0] = index_of_file;
			A[1] = nr_of_slave_files;
			MPI_Send(A, 2, MPI_INT,i, tag, MPI_COMM_WORLD);
			printf("ROOT> Am trimis %d si %d catre %d.\n",A[0],A[1],i);

			if(r>0) r--;	
			index_of_file = index_of_file + nr_of_slave_files;
			nr_of_slave_files = d + (int)(r>0);

			/******************************************************/
			/************ Create directories for the slaves *******/
			/******************************************************/
			char temp2[80];
			strcpy(temp,full_path);
			sprintf(temp2,"%d",i);
			strcat(temp, temp2);
			sprintf(temp2,"mkdir ./%s",temp);
			system(temp2);

			/******************************************************/
		}
		/******************************************************/
	}
	else{	//slaves	
		MPI_Recv(A, 2, MPI_INT, 0, tag,MPI_COMM_WORLD, &status);
		//printf("P%d> Am primit %d si %d.\n",my_rank,A[0],A[1]);
		index_of_file = A[0];
		nr_of_slave_files = A[1];
		////// ASTA E OK NU STERGE, se completeaza path`ul 
				////////////   cu numele folderelor pt fiecare slave
		sprintf(temp,"%d",my_rank);
		strcat(full_path, temp);
		//////////////////////////////////////

		// 
	}
	MPI_Barrier(MPI_COMM_WORLD);
	return A;
}

/***************************************************
****************** MAPPING PHASE *******************
***************************************************/
void* parsing(FILE* file)
{
	
	char line[100];
	char terms[30][100];
	int terms_count[30];
	int nr_of_terms = 0;
	int i;
	for(i=0;i<30;i++)
	{
		strcpy(terms[i],"\0");
		terms_count[i] = 0;
	}

	while((fscanf(file,"%s",line)) != EOF)
	{
		for(i=0;i<nr_of_terms;i++)
		{
			if(strstr(terms[i],line) != 0)
			{
				break;
			}
		}
		
		// if new word is found then it must be saved
		if(i >= nr_of_terms)
		{
			strcpy(terms[i],line);
			nr_of_terms ++ ;
		}
		// this needs to be increased:
		//	if new word then it must be 1
		//	else it means we found again a word we already had so 
		//			so it needs to be increased
		terms_count[i] ++;

	}

	parsed_data* ret = (parsed_data*)malloc(sizeof(parsed_data));
	
	ret->terms = (char**)malloc(nr_of_terms*sizeof(char*));
	ret->count = (int*)malloc(nr_of_terms*sizeof(int));
	for(i=0;i<nr_of_terms;i++)
	{
		ret->terms[i] = (char*)malloc(strlen(terms[i])*sizeof(char) + 1);
		strcpy(ret->terms[i],terms[i]);
		ret->count[i] = terms_count[i];
	}
	ret->nr_of_terms = nr_of_terms;
	return (void*)ret;
}

void generate_maped_data(parsed_data *parsed_file,char* file_parsed)
{
	FILE *temp_pfile;
	char temp[80];

	//printf("%d parsed files nr of terms %d\n",my_rank,parsed_file->nr_of_terms);
	for(int i=0;i<parsed_file->nr_of_terms;i++)
	{

		sprintf(temp,"./%s/%s",full_path,parsed_file->terms[i]);
		temp_pfile = fopen(temp,"a");
		fprintf(temp_pfile,"%s %d\n",file_parsed,parsed_file->count[i]);

		fclose(temp_pfile);
	}

}
void mapping()
{
	if(my_rank != 0)
	{
		parsed_data** parsed_file = (parsed_data**)malloc(nr_of_slave_files * sizeof(parsed_data*));
		FILE** slave_files = (FILE**)malloc(nr_of_slave_files * sizeof(FILE*));

		for(int i=index_of_file;i<index_of_file+nr_of_slave_files;i++)
		{
			int temp_index = i - index_of_file;
			slave_files[temp_index] = fopen(files[i],"r");
			if(slave_files[temp_index] == nullptr)
			{
				printf("! Procesorul %d nu poate deschide fisierul %s!\n",my_rank,files[i]);
			}
		}

		/************ HERE SHOULD BE THE PARSING OF THE FILES *****************/
		for(int i=0;i<nr_of_slave_files;i++)
		{
			parsed_file[i] = parsing(slave_files[i]);
		}
		/**********************************************************************/

		/****************** Generate the maped files***********************/
		for(int i=0;i<nr_of_slave_files;i++)
		{
			generate_maped_data(parsed_file[i],files[i]);
		}
		/**************************************************/



		// close the files which i had read
		for(int i=0;i<nr_of_slave_files;i++)
		{
			fclose(slave_files[i]);
		}

	}

}
/**************************************************/

int main(int argc, char* argv[]){	

	/***************************************************
	************* SETUP MPI ENVIRONENT *****************
	***************************************************/
	/* start up MPI */
	MPI_Init(&argc, &argv);
	
	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 
	
	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &p); 
	/****************************************************/
	
	////////// create the correct files path
	char temp[80];
	for(int i=0;i<nr_of_files;i++)
	{
		strcpy(temp,"./");
		strcat(temp,path);
		strcat(temp,"/");
		strcat(temp,files[i]);
		strcpy(files[i],temp);
	}
	///////////////////////////////////////
	
	/*****************************************************
	/***************** DISPATCH TASKS ********************
	*****************************************************/
	dispatch_tasks();
	/*****************************************************/
	MPI_Barrier(MPI_COMM_WORLD);
	mapping();





	/* shut down MPI */
	MPI_Finalize(); 
	
	
	return 0;
}
