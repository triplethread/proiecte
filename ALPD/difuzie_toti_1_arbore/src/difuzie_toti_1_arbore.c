/*
 ============================================================================
 Name        : difuzie_1_toti_arbore.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello MPI World in C 
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include "mpi.h"

int main(int argc, char* argv[]) {
	int my_rank; /* rank of process */
	int p; /* number of processes */
	char message[10][100]; /* storage for message */
	MPI_Status status; /* return status for receive */

	/* start up MPI */

	MPI_Init(&argc, &argv);
	/*
	 * Date intrare
	 */
	int nnodes = 6;
	int index[] = {2,3,7,8,9,10};
	int edges[] = { 1,2,0,3,4,5,0,2,2,2};
	int reorder = 1;
	int my_neighbors_count;
	int my_neighbors[10];
	MPI_Comm vu=NULL;
	// cream graful
	MPI_Graph_create(MPI_COMM_WORLD, nnodes, index, edges, reorder, &vu);

	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &p);

	MPI_Graph_neighbors(vu,  my_rank,10, my_neighbors);

	MPI_Graph_neighbors_count(vu, my_rank, &my_neighbors_count);
char root_message[10][100];
	if (my_rank == 0) {
		
		for(int i=0;i<my_neighbors_count;i++)
		{
			MPI_Recv(root_message[i],100,MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,vu,&status);
			fprintf(stdout,"<<Procesorul %d a primit mesajul de la procesorul %d.\n",my_rank,my_neighbors[i]);
		}

	}
	// frunzele doar trimit mesajul
	else if(my_neighbors_count==1)
	{
		char message[100];
		sprintf(message,"Hello from %d",my_rank);
		MPI_Send(message,strlen(message)+1,MPI_CHAR,my_neighbors[0],0,vu);
		fprintf(stdout,">>Procesorul %d a trimis mesaj la procesorul %d.\n",my_rank,my_neighbors[0]);
	}
	// noduri intermediare
	else{
		char to_send[100]="";
		sprintf(to_send,"Hello from %d",my_rank);
		int my_neighbors_value = 0;
		for (int i= 0;i<my_neighbors_count;i++)
		{
			my_neighbors_value += my_neighbors[i];
		}
		
		for(int i=0;i<my_neighbors_count-1;i++)
		{			
			
			MPI_Recv(message[i],100,MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,vu,&status);
			int source = status.MPI_SOURCE;
			fprintf(stdout,"<<Procesorul %d a primit mesajul de la procesorul %d.\n",my_rank,source);
			my_neighbors_value -= source;	
			sprintf(to_send,"%s %s",to_send,message[i]);
		}

		MPI_Send(to_send,strlen(to_send)+1,MPI_CHAR,my_neighbors_value,0,vu);
		fprintf(stdout,">>Procesorul %d a trimis mesaj la procesorul %d.\n",my_rank,my_neighbors_value);
	
		
	}
	MPI_Barrier(vu);
	if(my_rank == 0)
	{
		printf("%s \n %s \n",root_message[0],root_message[1]);
	}
	/* shut down MPI */
	MPI_Finalize();

	return 0;
}
