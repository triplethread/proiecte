/*
 ============================================================================
 Name        : difuzie_1_toti_arbore.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello MPI World in C 
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include "mpi.h"

int main(int argc, char* argv[]) {
	int my_rank; /* rank of process */
	int p; /* number of processes */
	char message[10][100]; /* storage for message */
	MPI_Status status; /* return status for receive */

	/* start up MPI */

	MPI_Init(&argc, &argv);
	/*
	 * Date intrare
	 */
	int diametru = 4;
	int nnodes = 6;
	int index[] = {3,5,6,7,8,10};
	int edges[] = {2,3,5,4,5,0,0,1,0,1};
	int reorder = 1;
	int my_neighbors_count;
	int my_neighbors[10];
	MPI_Comm vu=NULL;

	MPI_Graph_create(MPI_COMM_WORLD, nnodes, index, edges, reorder, &vu);

	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	MPI_Comm_size(MPI_COMM_WORLD, &p);

	MPI_Graph_neighbors(vu,  my_rank,10, my_neighbors);

	MPI_Graph_neighbors_count(vu, my_rank, &my_neighbors_count);
	char root_message[10][100];
	int max = my_rank;
	int dump=0;
	
	for(int i=0;i<diametru;i++)
	{
		for(int j=0;j<my_neighbors_count;j++){
			MPI_Send(&max,1,MPI_INT,my_neighbors[j],0,vu);
			//fprintf(stdout,">>%d<<Procesorul %d a trimis mesajul catre procesorul %d (%d).\n",i,my_rank,my_neighbors[j],max);
		}
		for(int j=0;j<my_neighbors_count;j++){
			MPI_Recv(&dump,1,MPI_INT,MPI_ANY_SOURCE, MPI_ANY_TAG,vu,&status);
			if(dump>max)
			{
				max = dump;
			}
		}
	}
	if(max == my_rank)
	{
		fprintf(stdout,"I`m leader %d\n",my_rank);
	}
	else
	{
		fprintf(stdout, "Not Leader! %d\n",my_rank);
	}
	/* shut down MPI */
	MPI_Finalize();

	return 0;
}