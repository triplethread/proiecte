
/*
 ============================================================================
 Name        : laborator6_openmp.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello OpenMP World in C
 ============================================================================
 */
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/**
 * Hello OpenMP World prints the number of threads and the current thread id
 */
int main(int argc, char *argv[]) {

	int numThreads, tid;
	int m = 3;
	int n = 8, n_cop = 4;
	int v[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
	int a[15];
	for(int i=8;i<=15;i++)
	{
		a[i]=v[i-8];
	}

	/* This creates a team of threads; each thread has own copy of variables  */
	for (int k = m - 1; k >= 0; k--) {
#pragma omp parallel
		{
		{
			tid = omp_get_thread_num();
			if (tid >= n_cop && tid < n_cop * 2) {
				a[tid]=0;
				//a[tid] = a[2 * tid] + a[2 * tid + 1];
				//printf("tid = %d aduna %d cu %d\n",tid,a[2*tid],a[2*tid+1]);
			}

		}

#pragma omp barrier

		}
		if(tid == 0)
			{
				n_cop /= 2;
			}
	}
	if(omp_get_thread_num() == 0)
	{
		printf("The sum of numbers = %d\n\n\n", n_cop);
		for(int i=0;i<16;i++)
		{
			printf("%d\n",a[i]);
		}
	}
		return 0;
}

